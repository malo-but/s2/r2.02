import java.awt.GridLayout;

import javax.swing.*;

public class CharacterSheetToolBar extends JToolBar{

    // Les boutons que l'on veut utiliser :
    // nouveau, charger, enregistrer, enregistrer sous
    private JButton newButton;
    private JButton loadButton;
    private JButton saveButton;
    private JButton saveAsButton;

    public CharacterSheetToolBar(CharacterSheetListener characterSheetListener){
        
        this.initComponents(characterSheetListener);
        
    }
    
    private void initComponents(CharacterSheetListener listener) {
        
        this.setOrientation(VERTICAL);
        this.setLayout(new GridLayout(4, 1));

        this.newButton = new JButton();
        this.loadButton = new JButton();
        this.saveButton = new JButton();
        this.saveAsButton = new JButton();

        this.newButton.setText("New");
        this.loadButton.setText("Load");
        this.saveButton.setText("Save");
        this.saveAsButton.setText("Save as");

        this.newButton.addActionListener(listener);
        this.loadButton.addActionListener(listener);
        this.saveAsButton.addActionListener(listener);
        this.saveButton.addActionListener(listener);

        add(this.newButton);
        add(this.loadButton);
        add(this.saveButton);
        add(this.saveAsButton);

    }
    
}
