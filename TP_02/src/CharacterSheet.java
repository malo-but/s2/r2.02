import javax.swing.*;
import java.awt.*;

public class CharacterSheet extends JFrame {

    private JPanel panel;

    private CharacterSheetStats characterSheetStats;
    private CharacterSheetToolBar characterSheetToolBar;
    private CharacterSheetStateBar characterSheetStateBar;

    private CharacterSheetListener characterSheetListener;

    private CharacterSheetModel characterSheetModel;

    public CharacterSheet() {
        this.initComponents();
    }

    private void initComponents() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new BorderLayout());

        this.characterSheetModel = new CharacterSheetModel();
        this.characterSheetListener = new CharacterSheetListener(this);
        this.characterSheetStateBar = new CharacterSheetStateBar("0.0.1");
        this.characterSheetToolBar = new CharacterSheetToolBar(this.characterSheetListener);
        this.characterSheetStats = new CharacterSheetStats(this.characterSheetListener);

        this.characterSheetToolBar.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
        this.characterSheetStats.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
        this.characterSheetStateBar.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));

        add(this.characterSheetToolBar, BorderLayout.WEST);
        add(this.characterSheetStats);
        add(this.characterSheetStateBar, BorderLayout.SOUTH);
        
    }

    public CharacterSheetStats getCharacterSheetStats(){
        return this.characterSheetStats;
    }

    public CharacterSheetToolBar getCharacterSheetToolBar(){
        return this.characterSheetToolBar;
    }

    public CharacterSheetStateBar getCharacterSheetStateBar(){
        return this.characterSheetStateBar;
    }

    public CharacterSheetModel getCharacterSheetModel(){
        return this.characterSheetModel;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                CharacterSheet characterSheet = new CharacterSheet();
                characterSheet.pack();
                characterSheet.setVisible(true);
            }
        });
    }

}
