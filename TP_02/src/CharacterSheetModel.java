enum Races { // On peut en ajouter d'autres
    Elfe, Hobbit, Homme, Nain
}

enum Classes { // On peut en ajouter d'autres
    Barde, Mage, Paladin, Ranger
}

public class CharacterSheetModel {

    private String CharacterName;
    private String CharacterGender;

    private Races CharacterRace;
    private Classes CharacterClass;

    private int baseCharPoints;
    private int minCharPoints;
    private int maxCharPoints;
    private int totalMaxCharPoints;
    private int currentCharPoints;

    public CharacterSheetModel(){
        this.initValues();
    }

    private void initValues() {
        
        this.CharacterName = "";
        this.CharacterGender = "Male";
        this.CharacterRace = Races.Nain;
        this.CharacterClass = Classes.Mage;

        this.baseCharPoints = 8;
        this.minCharPoints = 8;
        this.maxCharPoints = 15;
        this.totalMaxCharPoints = 75;
        
    }

    public String getCharacterName() {
        return this.CharacterName;
    }

    public String getCharacterGender() {
        return this.CharacterGender;
    }

    public Races getCharacterRace() {
        return this.CharacterRace;
    }

    public Classes getCharacterClass() {
        return this.CharacterClass;
    }

    public int getBaseCharPoints() {
        return this.baseCharPoints;
    }

    public int getMinCharPoints() {
        return this.minCharPoints;
    }

    public int getMaxCharPoints() {
        return this.maxCharPoints;
    }

    public int getTotalMaxCharPoints() {
        return this.totalMaxCharPoints;
    }

}
