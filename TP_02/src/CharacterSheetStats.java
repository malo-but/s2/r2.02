import javax.swing.*;
import javax.swing.event.ChangeListener;

import java.awt.*;

public class CharacterSheetStats extends JPanel {

    // On a besoin de ce blankLabel pour "passer" des cellules du GridLayout
    private JLabel blankLabel;

    // Les labels
    private JLabel nameLabel;
    private JLabel raceLabel;
    private JLabel classLabel;

    // Les champs de texte
    private JTextField nameTextField;

    // Les boutons radios
    private JRadioButton maleRadioButton;
    private JRadioButton femaleRadioButton;

    // Les combobox
    private JComboBox<Races> raceComboBox;
    private JComboBox<Classes> classComboBox;

    // Les spinners
    private JSpinner strengthSpinner;
    private JSpinner dexteritySpinner;
    private JSpinner constitutionSpinner;
    private JSpinner intelligenceSpinner;
    private JSpinner wisdomSpinner;
    private JSpinner charismaSpinner;
    private JLabel totalPointsLabel;

    // Model
    private CharacterSheetModel characterSheetModel;

    /**
     * Constructeur
     * @param characterSheetListener 
     */
    public CharacterSheetStats(CharacterSheetListener characterSheetListener) {

        this.initComponents(characterSheetListener);

    }

    private void initComponents(CharacterSheetListener listener) {
        this.blankLabel = new JLabel("");

        this.setLayout(new GridLayout(0, 2));

        /**
         * On ajoute les composants
         */
        this.nameLabel = new JLabel();
        this.nameLabel.setText(" Name :");
        add(this.nameLabel);

        this.nameTextField = new JTextField();
        add(this.nameTextField);

        this.raceLabel = new JLabel();
        this.raceLabel.setText(" Race :");
        add(this.raceLabel);

        this.raceComboBox = new JComboBox<Races>(Races.values());
        this.raceComboBox.setSelectedItem(listener.getBaseRace());
        add(this.raceComboBox);

        this.classLabel = new JLabel();
        this.classLabel.setText(" Class :");
        add(this.classLabel);

        this.classComboBox = new JComboBox<Classes>(Classes.values());
        this.classComboBox.setSelectedItem(listener.getBaseClass());
        add(this.classComboBox);

        this.maleRadioButton = new JRadioButton();
        this.femaleRadioButton = new JRadioButton();
        
        ButtonGroup radioGroup = new ButtonGroup();
        radioGroup.add(this.maleRadioButton);
        radioGroup.add(this.femaleRadioButton);
        
        JPanel radioPanel = new JPanel();

        radioPanel.add(this.maleRadioButton);
        this.maleRadioButton.setText("Male");

        radioPanel.add(this.femaleRadioButton);
        this.femaleRadioButton.setText("Female");

        if (listener.getGender() == "Male")
            this.maleRadioButton.setSelected(true);
        else
            this.femaleRadioButton.setSelected(true);

        add(radioPanel);

        add(this.blankLabel);

        JPanel characteristicsPanel = new JPanel();
        characteristicsPanel.setLayout(new GridLayout(0, 4));

        int baseCharPoints = listener.getBaseCharPoints();
        int minCharPoints = listener.getMinCharPoints();
        int maxCharPoints = listener.getMaxCharPoints();

        this.strengthSpinner = new JSpinner();
        this.intelligenceSpinner = new JSpinner();
        this.constitutionSpinner = new JSpinner();
        this.wisdomSpinner = new JSpinner();
        this.dexteritySpinner = new JSpinner();
        this.charismaSpinner = new JSpinner();
        this.totalPointsLabel = new JLabel();

        JLabel strengthLabel = new JLabel();
        strengthLabel.setText(" Str :");
        characteristicsPanel.add(strengthLabel);

        this.strengthSpinner.setModel(new SpinnerNumberModel(baseCharPoints, minCharPoints, maxCharPoints, 1));
        this.strengthSpinner.addChangeListener(listener);
        characteristicsPanel.add(this.strengthSpinner);

        JLabel intelligenceLabel = new JLabel();
        intelligenceLabel.setText(" Int :");
        characteristicsPanel.add(intelligenceLabel);

        this.intelligenceSpinner.setModel(new SpinnerNumberModel(baseCharPoints, minCharPoints, maxCharPoints, 1));
        this.intelligenceSpinner.addChangeListener(listener);
        characteristicsPanel.add(this.intelligenceSpinner);

        JLabel constitutionLabel = new JLabel();
        constitutionLabel.setText(" Con :");
        characteristicsPanel.add(constitutionLabel);

        this.constitutionSpinner.setModel(new SpinnerNumberModel(baseCharPoints, minCharPoints, maxCharPoints, 1));
        this.constitutionSpinner.addChangeListener(listener);
        characteristicsPanel.add(this.constitutionSpinner);

        JLabel wisdomLabel = new JLabel();
        wisdomLabel.setText(" Wis :");
        characteristicsPanel.add(wisdomLabel);

        this.wisdomSpinner.setModel(new SpinnerNumberModel(baseCharPoints, minCharPoints, maxCharPoints, 1));
        this.wisdomSpinner.addChangeListener(listener);
        characteristicsPanel.add(this.wisdomSpinner);

        JLabel dexterityLabel = new JLabel();
        dexterityLabel.setText(" Dex :");
        characteristicsPanel.add(dexterityLabel);

        this.dexteritySpinner.setModel(new SpinnerNumberModel(baseCharPoints, minCharPoints, maxCharPoints, 1));
        this.dexteritySpinner.addChangeListener(listener);
        characteristicsPanel.add(this.dexteritySpinner);   

        JLabel charismaLabel = new JLabel();
        charismaLabel.setText(" Cha :");
        characteristicsPanel.add(charismaLabel);

        this.charismaSpinner.setModel(new SpinnerNumberModel(baseCharPoints, minCharPoints, maxCharPoints, 1));
        this.charismaSpinner.addChangeListener(listener);
        characteristicsPanel.add(this.charismaSpinner);

        JLabel totalLabel = new JLabel();
        totalLabel.setText(" Total :");
        characteristicsPanel.add(totalLabel);

        this.totalPointsLabel.setText(this.getTotalCharPoints() + "");
        characteristicsPanel.add(this.totalPointsLabel);

        this.add(characteristicsPanel);

    }

    /**
     * Retourne le nombre maximum de points
     */
    public int getTotalCharPoints() {
        int res = (int) this.strengthSpinner.getValue() + (int) this.intelligenceSpinner.getValue()
                + (int) this.constitutionSpinner.getValue() + (int) this.wisdomSpinner.getValue()
                + (int) this.dexteritySpinner.getValue() + (int) this.charismaSpinner.getValue();

        return res;
    }

    /**
     * Met à jour le nombre de points
     */
    public void setTotalCharPoints(int totalCharPoints) {
        this.totalPointsLabel.setText(totalCharPoints + "");
    }

}
