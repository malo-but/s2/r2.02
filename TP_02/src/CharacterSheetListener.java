import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class CharacterSheetListener implements ActionListener, ChangeListener {

    private CharacterSheet characterSheet;
    private CharacterSheetModel characterSheetModel;

    public CharacterSheetListener(CharacterSheet characterSheet) {
        this.characterSheet = characterSheet;
        this.characterSheetModel = this.characterSheet.getCharacterSheetModel();
    }

    public void actionPerformed(ActionEvent paramActionEvent) {
        this.characterSheet.getCharacterSheetStateBar().setMessage(paramActionEvent.getActionCommand());
    }

    public void stateChanged(ChangeEvent evt) {
        
        this.characterSheet.getCharacterSheetStats().setTotalCharPoints(this.characterSheet.getCharacterSheetStats().getTotalCharPoints());

        if (this.characterSheet.getCharacterSheetStats().getTotalCharPoints() > this.characterSheetModel.getTotalMaxCharPoints()) {

            this.characterSheet.getCharacterSheetStats().setTotalCharPoints(this.characterSheetModel.getTotalMaxCharPoints());

        }

    }

    public Races getBaseRace() {
        return this.characterSheetModel.getCharacterRace();
    }

    public Classes getBaseClass() {
        return this.characterSheetModel.getCharacterClass();
    }

    public String getGender() {
        return this.characterSheetModel.getCharacterGender();
    }

    public int getBaseCharPoints() {
        return this.characterSheetModel.getBaseCharPoints();
    }

    public int getMinCharPoints() {
        return this.characterSheetModel.getMinCharPoints();
    }

    public int getMaxCharPoints() {
        return this.characterSheetModel.getMaxCharPoints();
    }

    public int getTotalMaxCharPoints() {
        return this.characterSheetModel.getTotalMaxCharPoints();
    }

}
