import javax.swing.*;
import java.awt.*;

public class CharacterSheetStateBar extends JPanel{

    // Les deux labels de notre barre d'état
    // le label de message et le label de version
    private JLabel messageLabel;
    private JLabel versionLabel;

    public CharacterSheetStateBar(String version){

        this.setLayout(new BorderLayout());

        this.initComponents(version);

    }

    private void initComponents(String version) {

        this.messageLabel = new JLabel();
        this.versionLabel = new JLabel();

        this.messageLabel.setText("");
        this.versionLabel.setText(version);

        add(this.messageLabel, BorderLayout.WEST);
        add(this.versionLabel, BorderLayout.EAST);

    }

    public void setMessage(String message){
        this.messageLabel.setText(message);
    }

}
