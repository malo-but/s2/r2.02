import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class Evaluation implements Serializable {

	private int coefficient;

	private double note;

	public Evaluation(int coeff, double note) {

		if (coeff >= 0 && coeff <= 10)
			this.coefficient = coeff;
		else {
			System.out.println("Coeff non compris entre 0 et 10 => mis à 1 par défaut.");
			this.coefficient = 1;
		}

		if (note >= 0 && note <= 20)
			this.note = note;
		else {
			System.out.println("Note non comprise entre 0 et 20 => mise à 1 par défaut.");
			this.note = 1;
		}

	}

	public Evaluation(String filePath) {
		
		ArrayList<String> lignes = new ArrayList<String>();

		boolean erreur = false;

		if (filePath != null) {

			try {

				Scanner input = new Scanner(new FileReader(filePath));

				while (input.hasNextLine()) {
					lignes.add(input.nextLine());
				}

			} catch (FileNotFoundException e) {
				System.out.println("Erreur => \"" + filePath + "\"fichier introuvable.");
				erreur = true;
			}

		} else {
			System.out.println("Chemin null => aucune lecture effectuée.");
			erreur = true;
		}

		String[] eval = lignes.get(0).split(" ");
			
		if (eval.length == 2) {

			try {

				int coeff = Integer.parseInt(eval[0]);
				double note = Double.parseDouble(eval[1]);

				this.coefficient = coeff;
				this.note = note;

			} catch (NumberFormatException e) {
				System.out.println("Erreur => \"" + lignes.get(0) + "\" n'est pas une note valide.");
				erreur = true;
			}

		} else {
			System.out.println("Fichier \"" + filePath + "\" non conforme => aucune lecture effectuée.");
			erreur = true;
		}

		if (erreur) {
			this.coefficient = 1;
			this.note = 1;
		}

	}

	public Evaluation(DataInputStream input) {
		
		try {

			this.coefficient = input.readInt();
			this.note = input.readDouble();

		} catch (IOException e) {
			System.out.println("Erreur lors de la lecture dans le fichier binaire.");
		}
	}

	public int getCoeff() {
		return this.coefficient;
	}

	public double getNote() {
		return this.note;
	}

	public void saveToFile(PrintWriter output) {

		output.println(this.coefficient + " " + this.note);

	}

	public void saveToFile(String filePath, boolean append) {

		if (filePath != null) {

			try {

				PrintWriter output = new PrintWriter(new FileWriter(filePath, append));

				this.saveToFile(output);

				output.close();

			} catch (IOException e) {
				System.out.println("Erreur => \"" + filePath + "\"fichier introuvable.");
			}

		} else {
			System.out.println("Chemin null => aucune écriture effectuée.");
		}

	}

	public void saveToFileBinary(DataOutputStream output) {
		
		try {

			output.writeInt(this.coefficient);
			output.writeDouble(this.note);

		} catch (IOException e) {
			System.out.println("Erreur lors de l'écriture dans le fichier binaire.");
		}

	}

	public String toString() {
		return this.note + " (" + this.coefficient + ")";
	}
	
}
