import java.util.ArrayList;

public class Scenario {

	public static void main(String[] args) {
		new Scenario();
	}

	public Scenario() {

		Eleve jean = new Eleve("../data/Jean_Bouffort.txt");

		System.out.println(jean + "\n");
		
		jean.saveToFile();

		Evaluation note = new Evaluation("../data/note_1.txt");

		System.out.println(note + "\n");

		note.saveToFile("../data/note_2.txt", true);

		jean.addNote(note);

		jean.saveToFileBinary("../data/Jean_bin");

		Eleve jean2 = new Eleve("../data/Jean_bin");

		System.out.println(jean2 + "\n");

	}
	
}
