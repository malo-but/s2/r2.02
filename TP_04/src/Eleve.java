import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class Eleve implements Serializable {
	
	private String prenom;

	private String nom;

	private ArrayList<Evaluation> notes;

	public Eleve(String prenom, String nom, ArrayList<Evaluation> notes) {

		if (prenom != null)
			this.prenom = prenom;
		else {
			System.out.println("Prénom null => valeure par défaut attribuée.");
			this.prenom = "prenom";
		}

		if (nom != null)
			this.nom = nom;
		else {
			System.out.println("Nom null => valeure par défaut attribuée.");
			this.nom = "nom";
		}

		if (notes != null)
			this.notes = notes;
		else {
			System.out.println("Tableau de notes null => tableau vide attribué.");
			this.notes = new ArrayList<Evaluation>();
		}

	}

	public Eleve(String filePath) {

		if (filePath.contains(".txt")) {

			ArrayList<String> lignes = new ArrayList<String>();

			boolean erreur = false;
			
			if (filePath != null) {

				try {

					Scanner input = new Scanner(new FileReader(filePath));

					while (input.hasNextLine()) {
						lignes.add(input.nextLine());
					}

				} catch (FileNotFoundException e) {
					System.out.println("Erreur => \"" + filePath + "\"fichier introuvable.");
					erreur = true;
				}

			} else {
				System.out.println("Chemin null => aucune lecture effectuée.");
				erreur = true;
			}

			if (lignes.size() >= 2) {
				
				this.prenom = lignes.get(0);
				this.nom = lignes.get(1);

				this.notes = new ArrayList<Evaluation>();

				for (int i = 2; i < lignes.size(); i++) {

					String[] eval = lignes.get(i).split(" ");

					if (eval.length == 2) {

						try {

							int coeff = Integer.parseInt(eval[0]);
							double note = Double.parseDouble(eval[1]);

							this.notes.add(new Evaluation(coeff, note));

						} catch (NumberFormatException e) {
							System.out.println("Erreur => \"" + lignes.get(i) + "\" n'est pas une note valide.");
							erreur = true;
						}

					} else {
						System.out.println("Erreur => \"" + lignes.get(i) + "\" n'est pas une note valide.");
						erreur = true;
					}

				}

			} else {
				System.out.println("Erreur => \"" + filePath + "\" ne contient pas assez de lignes.");
				erreur = true;
			}

			if (erreur) {
				this.prenom = "prenom";
				this.nom = "nom";
				this.notes = new ArrayList<Evaluation>();
			}
		
		} else {
			this.loadFromFileBinary(filePath);
		}

	}

	public void loadFromFileBinary(String filePath) {
		
		if (filePath != null) {

			try {

				DataInputStream input = new DataInputStream(new FileInputStream(filePath));

				this.prenom = input.readUTF();
				this.nom = input.readUTF();

				this.notes = new ArrayList<Evaluation>();

				while (input.available() > 0) {
					this.notes.add(new Evaluation(input));
				}

				input.close();

			} catch (IOException e) {
				System.out.println("Erreur => \"" + filePath + "\"fichier introuvable.");
			}

		} else 
			System.out.println("Chemin null => aucune lecture effectuée.");

	}

	public String getPrenom() {
		return this.prenom;
	}

	public String getNom() {
		return this.nom;
	}

	public ArrayList<Evaluation> getNotes() {
		return this.notes;
	}

	public void addNote(Evaluation eval) {
		if (eval != null) 
			this.notes.add(eval);
		else {
			System.out.println("Evaluation null => aucune note ajoutée.");
		}
	}

	public double getMoyenne() {

		int sommeCoeff = 0;
		double noteXcoeff = 0;

		for (Evaluation eval : notes) {
			sommeCoeff += eval.getCoeff();
			noteXcoeff += eval.getNote() * eval.getCoeff();
		}

		return noteXcoeff / sommeCoeff;
	}

	public void saveToFile(String filePath) {

		if (filePath != null) {

			try {

				PrintWriter output = new PrintWriter(new FileWriter(filePath));

				output.println(this.prenom);
				output.println(this.nom);

				for (Evaluation eval : this.notes) {
					eval.saveToFile(output);
				}

				output.close();

			} catch (IOException e) {
				System.out.println("Erreur => \"" + filePath + "\"fichier introuvable.");
			}

		} else 
			System.out.println("Chemin null => aucune écriture effectuée.");

	}

	public void saveToFile() {
		this.saveToFile("../data/" + this.prenom + "_" + this.nom + ".txt");
	}

	public void saveToFileBinary(String filePath) {

		if (filePath != null) {

			try {

				DataOutputStream output = new DataOutputStream(new FileOutputStream(filePath));

				output.writeUTF(this.prenom);
				output.writeUTF(this.nom);

				for (Evaluation eval : this.notes) {
					eval.saveToFileBinary(output);
				}

				output.close();

			} catch (IOException e) {
				System.out.println("Erreur => \"" + filePath + "\"fichier introuvable.");
			}

		} else 
			System.out.println("Chemin null => aucune écriture effectuée.");

	}

	public String toString() {
		
		String str = this.prenom + " " + this.nom + "\n";

		for (Evaluation eval : this.notes) {
			str += eval.toString() + "\n";
		}

		str += "Moyenne : " + this.getMoyenne();

		return str;
	}

}
