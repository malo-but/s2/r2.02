package chrono;

import java.awt.*;
import javax.swing.*;

/**
 * Main class for the chrono application
 */
public class ChronoApp extends JFrame {
	
	/**
	 * The listener used throughout the app
	 */
	private ChronoListener chronoListener;

	/**
	 * The vue used throughout the app
	 */
	private ChronoVue chronoVue;

	/**
	 * The main entry point
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ChronoApp chronoApp = new ChronoApp();
                chronoApp.pack();
                chronoApp.setVisible(true);
            }
        });
    }

	/**
	 * Basic constructor
	 */
	public ChronoApp() {
		this.initializeComponents();
	}

	/**
	 * Method used to initialize the different class components
	 */
	private void initializeComponents() {

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());

		this.chronoListener = new ChronoListener(this);
		this.chronoVue = new ChronoVue(this.chronoListener);

		this.add(this.chronoVue);

	}

	/**
	 * Updates the vue components
	 * @param listener The listener
	 */
	public void updateVue(ChronoListener listener) {
		this.chronoVue.updateComponents(listener);
	}

	/**
	 * Acts on the timer depending on the input string
	 * @param listener The listener
	 * @param action The action to perform
	 */
	public void actOnTimer(ChronoListener listener, String action) {
		this.chronoVue.actOnTimer(listener, action);
	}

}
