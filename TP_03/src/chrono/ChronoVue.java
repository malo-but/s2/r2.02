package chrono;

import java.awt.*;
import javax.swing.*;
import java.text.SimpleDateFormat;
import java.awt.GridLayout;

/**
 * Class handling all visual components
 */
public class ChronoVue extends JPanel {

	/**
	 * Timer object
	 */
	private javax.swing.Timer timer;

	/**
	 * The timer's visual representation
	 */
	private JLabel timerLabel;

	/**
	 * Object used to format milliseconds 
	 */
	private SimpleDateFormat dateFormat;

	/**
	 * Tool buttons
	 */
	private JPanel buttonPanel;

	/**
	 * Start and Stop button
	 */
	private JButton startStopButton;

	/**
	 * Reset button
	 */
	private JButton resetButton;

	/**
	 * Constructor
	 * @param listener The listener
	 */
	public ChronoVue(ChronoListener listener) {
		this.initializeComponents(listener);
	}

	/**
	 * Method used to initialize all of the class' components
	 * @param listener The listener
	 */
	private void initializeComponents(ChronoListener listener) {

		this.setLayout(new GridLayout(1, 2));

		this.timer = new Timer(listener.getDelay(), listener);
		this.timerLabel = new JLabel();
		this.dateFormat = new SimpleDateFormat("mm:ss");
		
		this.timerLabel.setText(this.dateFormat.format(listener.getTimeMs()));

		this.add(this.timerLabel);

		this.buttonPanel = new JPanel();
        this.buttonPanel.setLayout(new GridLayout(2, 1));

		this.startStopButton = new JButton("Start");
		this.resetButton = new JButton("Reset");

		this.startStopButton.addActionListener(listener);
		this.resetButton.addActionListener(listener);

		this.buttonPanel.add(this.startStopButton);
		this.buttonPanel.add(this.resetButton);

		this.add(this.buttonPanel);

	}

	/**
	 * Method used to update the visual components
	 * @param listener The listener
	 */
	public void updateComponents(ChronoListener listener) {
		this.timerLabel.setText(this.dateFormat.format(listener.getTimeMs()));
	}

	/**
	 * Method modifying the timer's state depending on the argument string
	 * @param listener The listener
	 * @param action The action
	 */
	public void actOnTimer(ChronoListener listener, String action) {

		switch (action) {
	
			case "Start":
			case "Stop":
				if (this.timer.isRunning()) {
					this.startStopButton.setText("Start");
					this.timer.stop();
					this.resetButton.setEnabled(true);
				} else {
					this.startStopButton.setText("Stop");
					this.timer.start();
					this.resetButton.setEnabled(false);
				}
				break;
			
			case "Reset":
				listener.resetTimer();
				break;
			
			default:
				break;

		}

	}
	
}
