package chrono;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Listener used to update data depending on user actions
 */
public class ChronoListener implements ActionListener {
	
	/**
	 * Instance of the main class, used to pass messages
	 */
	private ChronoApp chronoApp;

	/**
	 * The delay used by the timer
	 */
	private final static int DELAY = 1000;

	/**
	 * The current time spent in ms
	 */
	private int timeMs;

	/**
	 * Constructor initializing basic attributes
	 * @param chronoApp The main class
	 */
	public ChronoListener(ChronoApp chronoApp) {
		this.chronoApp = chronoApp;
		this.timeMs = 0;
	}

	/**
	 * Method implemented by the ActionListener interface
	 * Used to act on user action or timer action
	 */
    public void actionPerformed(ActionEvent paramActionEvent) {

		String buttonPressed = paramActionEvent.getActionCommand();

		if (buttonPressed != null) {

			this.chronoApp.actOnTimer(this, buttonPressed);
			this.chronoApp.updateVue(this);

		} else {	

			this.timeMs += DELAY;
			this.chronoApp.updateVue(this);

		}

	}

	/**
	 * Returns the time in ms
	 * @return The time in ms
	 */
	public int getTimeMs() {
		return this.timeMs;
	}

	/**
	 * Returns the delay used by the timer
	 * @return The delay used by the timer
	 */
	public int getDelay() {
		return DELAY;
	}

	/**
	 * Resets the timer time to 0
	 */
	public void resetTimer() {
		this.timeMs = 0;
	}

}
