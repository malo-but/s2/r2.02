package calendar;

import java.awt.event.*;

/**
 * Listener class used to perform actions depending on the user
 */
public class CalendarAppListener implements ItemListener, KeyListener {

	/**
	 * Main class object
	 */
	private CalendarApp calendarApp;

	/**
	 * The model used to retrieve data
	 */
	private Calendar calendarAppModel;

	/**
	 * Constructor initializing the class attributes
	 * @param calendarApp Instance of the app main class
	 */
	public CalendarAppListener(CalendarApp calendarApp) {
		this.calendarApp = calendarApp;
		this.calendarAppModel = this.calendarApp.getModel();
	}

	/**
	 * Method implemented by the ItemListener interface
	 * Used to update data depending on the month chosen by the user
	 * @param event The event passed by the user action
	 */
	public void itemStateChanged(ItemEvent event) {

		if (event.getStateChange() == ItemEvent.SELECTED) {

			String selectedMonth = event.getItem().toString();

			int i = 0;
			boolean found = false;

			while (i < this.getMonthsNames().length && !found) {
				if (this.getMonthsNames()[i].equals(selectedMonth)) {
					found = true;
				}
				i++;
			}

			this.calendarAppModel.setMonth(i);

			this.calendarApp.updateVue(this);

		}

	}

	/**
	 * Method implemented by the KeyListener inerface 
	 * Used to trigger different actions depending on which key the user pressed
	 * @param e The event passed by the user
	 */
	public void keyPressed(KeyEvent e) {

		if (e.getKeyCode() == 39) {
			this.calendarAppModel.nextDay();			
		} else if (e.getKeyCode() == 37) {
			this.calendarAppModel.previousDay();	
		}
			
		this.calendarApp.updateVue(this);
	}
	
	/**
	 * Method implemented by the KeyListener inerface 
	 * @param e The event passed by the user
	 */
	public void keyTyped(KeyEvent e) {}

	/**
	 * Method implemented by the KeyListener inerface 
	 * @param e The event passed by the user
	 */
	public void keyReleased(KeyEvent e) {}

	/**
	 * Returns the day name from the model
	 * @return The day name
	 */
	public String getDayName() {
		return this.calendarAppModel.getDayName();
	}

	/**
	 * Returns the day number from the model
	 * @return The day number
	 */
	public int getdayNumber() {
		return this.calendarAppModel.getDayNumber();
	}

	/**
	 * Returns the month name from the model
	 * @return The month name
	 */
	public String getMonthName() {
		return this.calendarAppModel.getMonthName();
	}

	/**
	 * Returns the year from the model
	 * @return The year
	 */
	public int getYear() {
		return this.calendarAppModel.getYear();
	}

	/**
	 * Returns a list of the different month names
	 * @return The months names
	 */
	public String[] getMonthsNames() {
		return this.calendarAppModel.getMonthsNames();
	}

}