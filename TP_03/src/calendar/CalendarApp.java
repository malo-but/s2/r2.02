package calendar;

import java.awt.*;
import javax.swing.*;

/**
 * Main class for the calendar application
 */
public class CalendarApp extends JFrame {

	/**
	 * The model used throughout the app
	 */
	private Calendar calendarAppModel;

	/**
	 * The listener used throughout the app
	 */
	private CalendarAppListener calendarAppListener;

	/**
	 * The vue used throughout the app
	 */
	private CalendarAppVue calendarAppVue;

	/**
	 * Main entry point
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                CalendarApp calendarApp = new CalendarApp();
                calendarApp.pack();
                calendarApp.setVisible(true);
            }
        });
    }

	/**
	 * Base constructor
	 */
	public CalendarApp() {
		this.initializeComponents();
	}

	/**
	 * Method used to initialize all the different class attributes
	 */
	private void initializeComponents() {

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());

		this.calendarAppModel = new Calendar();
		this.calendarAppListener = new CalendarAppListener(this);
		this.calendarAppVue = new CalendarAppVue(this.calendarAppListener);
		
		this.add(this.calendarAppVue);
		this.addKeyListener(this.calendarAppListener);

	}

	/**
	 * Returns the model
	 * @return the model
	 */
	public Calendar getModel() {
		return this.calendarAppModel;
	}

	/**
	 * Updates the vue components 
	 * @param listener The listener
	 */
	public void updateVue(CalendarAppListener listener) {
		this.calendarAppVue.updateComponents(listener);
	}

}