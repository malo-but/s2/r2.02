package calendar;

import java.awt.*;
import javax.swing.*;

/**
 * Class used to handle all visual components
 */
public class CalendarAppVue extends JPanel {

	/**
	 * Label holding the name of the current day
	 */
	private JLabel dayNameLabel;

	/**
	 * Label holding the number of the current day
	 */
	private JLabel dayNumberLabel;

	/**
	 * Combo box holding the different month names
	 */
	private JComboBox<String> monthNameCombo;

	/**
	 * Label holding the current year
	 */
	private JLabel yearLabel;

	/**
	 * Basic constructor
	 * @param listener The app listener
	 */
	public CalendarAppVue(CalendarAppListener listener) {
		this.initializeComponents(listener);
	}

	/**
	 * Methos used to initialize all of the class attributes 
	 * @param listener The app listener
	 */
	private void initializeComponents(CalendarAppListener listener) {

		this.setLayout(new FlowLayout());

		this.dayNameLabel = new JLabel();
		this.dayNumberLabel = new JLabel();
		this.monthNameCombo = new JComboBox<String>(listener.getMonthsNames());
		this.yearLabel = new JLabel();

		this.dayNameLabel.setText(listener.getDayName());
		this.dayNumberLabel.setText(listener.getdayNumber() + "");
		this.monthNameCombo.setSelectedItem(listener.getMonthName());
		this.yearLabel.setText(listener.getYear() + "");

		this.monthNameCombo.addItemListener(listener);
		this.monthNameCombo.setFocusable(false);

		this.add(this.dayNameLabel);
		this.add(this.dayNumberLabel);
		this.add(this.monthNameCombo);
		this.add(this.yearLabel);

	}

	/**
	 * Method used to update all the components content from the model
	 * @param listener The listener used to retrieve the data from the model
	 */
	public void updateComponents(CalendarAppListener listener) {
		 
		this.dayNameLabel.setText(listener.getDayName());
		this.dayNumberLabel.setText(listener.getdayNumber() + "");
		this.monthNameCombo.setSelectedItem(listener.getMonthName());
		this.yearLabel.setText(listener.getYear() + "");

	}

}