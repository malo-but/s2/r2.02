import javax.swing.*;

public class HelloWorldSwing extends JFrame {

    /**
     * Program entry point
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        SwingUtilities.invokeLater(new Runnable() {
            // Create and show this application's GUI.
            public void run() {
                // Create and set up the window.
                HelloWorldSwing frame = new HelloWorldSwing(args);
                // Size the window.
                frame.pack();
                // Display the window.
                frame.setVisible(true);
            }
        });
    }

    /** Initialize the HelloWorldSwing frame components. */
    public HelloWorldSwing(String[] param) {
        // Set the frame's title
        setTitle("HelloWorldSwing");
        // Set the frame's default close operation
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set the frame's layout
        getContentPane().setLayout(new java.awt.FlowLayout());

        // Create a new label for each command line argument
        for (String arg : param) {
            // Create a new label
            JLabel label = new JLabel(arg);
            // Add the label to the frame
            add(label);

        }
    }
}
