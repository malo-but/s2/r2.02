import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

/**
 * View class
 */
public class AppView extends JPanel {

	/**
	 * List of labels
	 */
	private ArrayList<JLabel> labels;

	/**
	 * Index of the red label in the list
	 */
	private int redLabel = -1;

	/**
	 * Constructor used to initialize components
	 * @param labels the list of string for the labels
	 */
	public AppView(ArrayList<String> labels) {
		this.initializeComponents(labels);
	}

	/**
	 * Used to initialize the different attributes
	 * @param labels the listener
	 */
	private void initializeComponents(ArrayList<String> labels) {

		this.setLayout(new GridLayout(0, 1));

		this.labels = new ArrayList<>();

		for (String label : labels) {
			this.labels.add(new JLabel(label));
		}

		for (JLabel label : this.labels) {
			this.add(label);
		}

		this.update();

	}

	/**
	 * Updates the view
	 */
	public void update() {

		this.redLabel += 1;

		if (this.redLabel == this.labels.size()) {
			this.redLabel = 0;
		}

		for (int i = 0; i < this.labels.size(); i++) {

			JLabel label = this.labels.get(i);

			if (i == this.redLabel) {
				label.setForeground(Color.red);
				this.add(label);
			} else {
				label.setForeground(Color.black);
				this.add(label);
			}
		}

	}
	
}
