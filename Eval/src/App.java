import java.awt.*;
import javax.swing.*;

import java.util.ArrayList;
import java.io.*;
import java.util.Scanner;

/**
 * Main class
 */
public class App extends JFrame{

	/**
	 * the App listener
	 */
	private AppListener listener;

	/**
	 * The App view
	 */
	private AppView view;

	/**
	 * Main entry point
	 * @param args command line arguements used to retrieve the file
	 */
	public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                App app = new App(args[0]);
                app.pack();
                app.setVisible(true);
            }
        });
    }

	/**
	 * Main constructor
	 * @param file the path of the file
	 */
	public App(String file) {
		System.out.println(file);
		this.initializeComponents(file);
	}

	/**
	 * Initializes the atributes
	 * @param file the path of the file
	 */
	private void initializeComponents(String file) {

		ArrayList<String> labels = this.getLabelList(file);

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());

		this.listener = new AppListener(this);
		this.view = new AppView(labels);

		this.add(this.view);
		this.addKeyListener(listener);

	}

	/**
	 * Reads a file
	 * @param file the file path
	 * @return a list of the file's lines
	 */
	private ArrayList<String> getLabelList(String file) {
		
		ArrayList<String> lignes = new ArrayList<String>();

		boolean erreur = false;
		
		if (file != null) {

			try {

				Scanner input = new Scanner(new FileReader(file));

				while (input.hasNextLine()) {
					lignes.add(input.nextLine());
				}

			} catch (FileNotFoundException e) {
				System.out.println("Erreur => \"" + file + "\"fichier introuvable.");
				erreur = true;
			}

		} else {
			System.out.println("Chemin null => aucune lecture effectuée.");
			erreur = true;
		}

		return lignes;

	}

	/**
	 * Updates the view
	 */
	public void updateView() {
		this.view.update();
	}
	
}
