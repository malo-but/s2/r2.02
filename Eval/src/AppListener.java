import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
 * KeyListener class to get the keys pressed
 */
public class AppListener implements KeyListener {

	/**
	 * The main instance
	 */
	private App app;

	/**
	 * Constuctor
	 * @param app the main instance
	 */
	public AppListener(App app) {
		this.app = app;
	}

	/**
	 * Method listening for keys being pressed
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == 83) {
			this.app.updateView();
		}	
	}

	/**
	 * Method implemented by the KeyListener inerface 
	 * @param e The event passed by the user
	 */
	public void keyTyped(KeyEvent e) {}

	/**
	 * Method implemented by the KeyListener inerface 
	 * @param e The event passed by the user
	 */
	public void keyReleased(KeyEvent e) {}
	
}
